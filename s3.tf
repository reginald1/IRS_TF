resource "aws_s3_bucket" "iep_bucket" {

  bucket = "${var.iep_app_s3_bucket}"
  acl = "private"
  
  tags = {
    Name = "iep_app_files"
    Environment = "${terraform.workspace}"

   
 }

}
