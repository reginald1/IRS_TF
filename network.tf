resource "aws_vpc" "iep_vpc" {
 cidr_block =  "${var.vpc_cidr}"
 instance_tenancy = "default"

 tags = {
   Name = "${local.vpc_name}"
   Environment = "${terraform.workspace}" 
   Location = "Maryland"

 }

}


resource "aws_internet_gateway" "igw" {
    vpc_id = "${aws_vpc.iep_vpc.id}"

    tags = {
        Name = "sample_igw"

    }
}


#########################PUBLIC_SUBNET

resource "aws_subnet" "public_subnet" {
  count = "${length(local.az_names)}"
  vpc_id = "${aws_vpc.iep_vpc.id}"
  map_public_ip_on_launch = true
  cidr_block = "${cidrsubnet(var.vpc_cidr, 8, count.index)}" ## "${cidersubnet("10.20.0.0/16"), 8, 1)}"
  availability_zone = "${data.aws_availability_zones.azs.names[count.index]}"
  tags = {
    Name = "public_subnets-${count.index + 1}"
  }

}
#############PUBLIC_ROUTE_TABLE
resource "aws_route_table" "public_routes" {
    vpc_id = "${aws_vpc.iep_vpc.id}"

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.igw.id}"

    }
    
    tags = {
        Name = "public route_subnet"
    }

}
#####################PUBLIC_ROUTE_TABLE_ASSOC



  resource "aws_route_table_association" "public_rt_association"{
        count = "${length(local.az_names)}"
        subnet_id = "${local.public_sub_IDS[count.index]}"
        route_table_id = "${aws_route_table.public_routes.id}"
    }

########################PRIVATE_SUBNET	
resource "aws_subnet" "private_subnet" {
    count = "${length(slice(local.az_names, 0, 2))}"
    vpc_id = "${aws_vpc.iep_vpc.id}"
    cidr_block = "${cidrsubnet(var.vpc_cidr, 8 , count.index + length(local.az_names))}"
    availability_zone = "${data.aws_availability_zones.azs.names[count.index]}"

    tags = {
        Name = "Private_subnets-${count.index + 1}"
    }
}


###########################PRIVATE_ROUTE_TABLE 

  
resource "aws_route_table" "private_routes" {
    vpc_id = "${aws_vpc.iep_vpc.id}"
    route {
    cidr_block = "0.0.0.0/0"
    instance_id = "${aws_instance.nat_instance.id}"
   }  
    
    
}


########################PRIVATE_ROUTE_TABLE_ASSOCIATION

resource "aws_route_table_association" "private_rt_association" {
    count = "${length(slice(local.az_names, 0, 2))}"
    subnet_id = "${aws_subnet.private_subnet.*.id[count.index]}"
    route_table_id = "${aws_route_table.private_routes.id}"

}

##############NAT_INSTANCE_SECURITY_GROUP_


resource "aws_security_group" "nat_sg" {
    name= "nat_sg"
    vpc_id = "${aws_vpc.iep_vpc.id}"

    egress {
      from_port = 0
      to_port   = 0 
      protocol = "-1"
     cidr_blocks = ["0.0.0.0/0"]
}

} 


resource "aws_security_group" "iep_web_sg" {
    name= "iep_app_sg"
    vpc_id = "${aws_vpc.iep_vpc.id}"

    ingress {
      from_port = 80
      to_port   = 80
      protocol = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
}
    ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]

}
     egress {
       from_port = 0
       to_port   = 0
       protocol = "-1"
       cidr_blocks = ["0.0.0.0/0"]
}

}
