variable "vpc_cidr" {
    type = "string"
    default = "10.20.0.0/16"
    
}

variable "amis_va" {
    type = "map"
    description = "us-east-1a is amazon linux 2 us-east1b is red hat 8 "
    default = {
      us-east-1 = "ami-0947d2ba12ee1ff75"
      us-east-2 = "ami-098f16afa9edf40be"
     } 
    }

variable "region" {
    default = "us-east-1"
    description = "region for application stack"
    type = "string"

}

variable "web_amis" {
    type = "map"
    default = {
        us-east-1 = "ami-0947d2ba12ee1ff75"
        us-east-2 = "ami-098f16afa9edf40be"
    }

}

variable "web_instance_type" {
    type = "string"
    description = "web instance type for application"
    default = "t2.micro"
}


variable "web_tags" {
    type = "map"
    default = {
        Name = "Webserver"
    }
}

variable "web_count" {
    type = "string"
    description = "this is for the instance amount"
    default = "2"
}

variable "iep_app_s3_bucket" {
  default = "iep-app-s3-bucket-dev"
}
