locals {
    vpc_name = "${terraform.workspace == "dev" ? "irs_vpc_dev" : "irs_vpc_prod"}"
}


locals {
    az_names = "${data.aws_availability_zones.azs.names}"
    public_sub_IDS = "${aws_subnet.public_subnet.*.id}"
}



locals {
    env_tag = {
        Enviroment = "dev"
    }
    web_tags = "${merge(var.web_tags, local.env_tag)}"
}



