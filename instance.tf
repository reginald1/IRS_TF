resource "aws_instance" "nat_instance" {
  ami           = "${var.amis_va[var.region]}"
  instance_type = "t3.micro"
  vpc_security_group_ids = ["${aws_security_group.nat_sg.id}"]
  subnet_id = "${local.public_sub_IDS[0]}"
  tags = {
    Name = "IEP_LINUX_VM"
  }
}


resource "aws_instance" "iep_app" {
    ami = "${var.web_amis[var.region]}"
    instance_type = "${var.web_instance_type}"
    subnet_id = "${local.public_sub_IDS[count.index]}"
    count = "${var.web_count}"
    iam_instance_profile = "${aws_iam_instance_profile.iep_s3_ec2_profile.name}"
    tags = "${local.web_tags}"
    user_data = "${file("apache.sh")}"
    vpc_security_group_ids = ["${aws_security_group.iep_web_sg.id}"]
}
