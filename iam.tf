resource "aws_iam_policy" "s3_ec2_policy" {
  name        = "test-policy"
  description = "A test policy"
  policy = "${data.template_file.s3_web_policy.rendered}" 

}

resource "aws_iam_role" "s3_ec2_role" {
  name = "s3_ec2_role"

  assume_role_policy = "${file("ec2_sts.json")}"

}

resource "aws_iam_role_policy_attachment" "iep_s3_ec2-policy-attachment" {
  role       = aws_iam_role.s3_ec2_role.name
  policy_arn = aws_iam_policy.s3_ec2_policy.arn
}

resource "aws_iam_instance_profile" "iep_s3_ec2_profile" {
  name = "iep_s3_ec2_profile"
  role = aws_iam_role.s3_ec2_role.name
}

