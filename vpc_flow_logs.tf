resource "aws_security_group" "nat_security_group" {
    name = "nat_security_group"
    description = "allow all traffic to private subnet"
    vpc_id = "${aws_vpc.iep_vpc.id}"

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

}

###############################VPC_FLOW_LOGS
resource "aws_flow_log" "iep_vpc_flow_logs" {
  iam_role_arn    = aws_iam_role.flow_role.arn
  log_destination = aws_cloudwatch_log_group.iep_CLG.arn
  traffic_type    = "ALL"
  max_aggregation_interval = "600"
  vpc_id = "${aws_vpc.iep_vpc.id}"
}

resource "aws_cloudwatch_log_group" "iep_CLG" {
  name = "iep_CLG"
}

resource "aws_iam_role" "flow_role" {
  name = "flow_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "IRS_vpc_role" {
  name = "IEP_FLOW_LOG_ROLE"
  role = aws_iam_role.flow_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

